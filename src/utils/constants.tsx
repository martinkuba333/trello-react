let host = "http://localhost:3000";

export const ENDPOINT = {
    //Board Endpoints
    GET_BOARD: `${host}/boards`,
    CREATE_BOARD: `${host}/boards/create`,
    DELETE_BOARD: `${host}/boards/delete`,
    UPDATE_BOARD: `${host}/boards/update`,

    // Column Endpoints
    GET_COLUMNS: (idBoard: number) => `${host}/boards/${idBoard}/column/get`,
    CREATE_COLUMN: (idBoard: number) => `${host}/boards/${idBoard}/column/create`,
    DELETE_COLUMN: (idBoard: number) => `${host}/boards/${idBoard}/column/delete`,
    UPDATE_COLUMN: (idBoard: number) => `${host}/boards/${idBoard}/column/update`,

    // Card Endpoints
    GET_CARDS: (idBoard: number, idColumn: number) => `${host}/boards/${idBoard}/column/${idColumn}/card/get`,
    CREATE_CARD: (idBoard: number, idColumn: number) => `${host}/boards/${idBoard}/column/${idColumn}/card/create`,
    DELETE_CARD: (idBoard: number, idColumn: number) => `${host}/boards/${idBoard}/column/${idColumn}/card/delete`,
    UPDATE_CARD: (idBoard: number, idColumn: number) => `${host}/boards/${idBoard}/column/${idColumn}/card/update`,
}
