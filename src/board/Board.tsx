import React, {useEffect, useState} from "react";
import axios from "axios";
import {ENDPOINT} from "../utils/constants";
import {BoardWrapper} from "./Board.styles";

const Board = () => {
    const [boards, setBoards] = useState(null);

    const getBoards = () => {
        axios.get(ENDPOINT.GET_BOARD)
            .then((response) => {
                setBoards(response.data);
            })
            .catch((e) => {
                console.log(e.message)
                setBoards(null);
            })
    }

    const showBoards = () => {
        const result: any[] = [];
        let childCounter = 0;
        // @ts-ignore
        boards.forEach((board: any) => {
            result.push(
                <div key={childCounter++}>
                    {board.id} - {board.title}
                </div>
            );
        });
        return result;
    }

    return (
        <>
            <BoardWrapper>
                <div onClick={getBoards}>
                    "click here"
                </div>
                {(boards) ?
                    showBoards()
                    : ""
                }
            </BoardWrapper>
        </>
    );

}

export default Board;
