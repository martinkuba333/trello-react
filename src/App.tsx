import React, {useEffect, useState} from "react";
import './App.css';
import Board from '../src/board/Board'

function App() {
    return (
        <div className="App">
            <Board/>
        </div>
    );
}

export default App;
